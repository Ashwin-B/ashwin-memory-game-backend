function dbQuery(connection, query) {
  return new Promise((resolve, reject) => {
    connection.query(query, (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

async function createDatabaseTable(connection) {
  try {
    const dropHighScoreTableQuery = "DROP TABLE IF EXISTS highscores";
    const highScoreTableQuery =
      "CREATE TABLE highscores(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(40), moves INT NOT NULL, PRIMARY KEY(id));";

    await dbQuery(connection, dropHighScoreTableQuery);
    await dbQuery(connection, highScoreTableQuery);
  } catch (error) {
    throw error;
  }
}

module.exports = { dbQuery, createDatabaseTable };
