const mysql = require("mysql");
const {
  mysql_database,
  mysql_host,
  mysql_name,
  mysql_password,
} = require("./config");

const connection = mysql.createPool({
  host: mysql_host,
  user: mysql_name,
  password: mysql_password,
  database: mysql_database,
});

module.exports = connection;
