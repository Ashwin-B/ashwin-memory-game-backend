const { mailgun_api, mailgun_domain } = require("./config");
const mailgun = require("mailgun-js")({
  apiKey: mailgun_api,
  domain: mailgun_domain,
});

function sendingMail(recepientMail) {
  let data = {
    from: "no-reply@ashwin-memorygame.com",
    to: recepientMail,
    subject: "Got Highscore in Ashwin's Memory Game",
    text:
      "Congratulations! You have completed the game by making lesser number of moves. Play more to be in top 10 leaderboard.",
  };

  mailgun.messages().send(data, (err, body) => {
    if (err) {
      console.error(err);
    } else {
      console.log(body);
    }
  });
}

module.exports = sendingMail;
