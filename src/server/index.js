const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const fs = require("fs");
const path = require("path");
const { port } = require("./config");
const highscores = require("./routes/highscores");

const app = express();

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "access.log"),
  { flags: "a" }
);

app.use(cors());
app.use(morgan("combined", { stream: accessLogStream }));
app.use(express.json());
app.use("/api/highscores", highscores);

app.use((req, res, next) => {
  res.status(404).send("Not found");
});

app.use((err, req, res, next) => {
  if (err) {
    res
      .status(500)
      .json({ error: "Cannot process your request. Please try again later." });
  } else {
    res.status(500).json({ error: "Internal Server error" });
  }
});

app.listen(port);
