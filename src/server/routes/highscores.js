const express = require("express");
const Joi = require("joi");
const connection = require("../dbConnection");
const { createDatabaseTable, dbQuery } = require("../createDatabaseTable");
const mailService = require("../mailService");

const router = express.Router();

createDatabaseTable(connection).catch((error) => {
  console.error(error.message);
});

router.get("/", (req, res, next) => {
  const highScoreQuery = "SELECT * FROM highscores ORDER BY moves";

  dbQuery(connection, highScoreQuery)
    .then((highscores) => {
      res.json(highscores);
    })
    .catch((error) => {
      next(error);
    });
});

const schema = Joi.object({
  name: Joi.string().min(3).required(),
  moves: Joi.number().integer().required(),
  email: Joi.string().email(),
});

router.post("/", (req, res, next) => {
  const addHighScoreQuery = `INSERT INTO highscores(name, moves) VALUES("${connection.escape(
    req.body.name
  )}", ${connection.escape(req.body.moves)})`;

  const { error } = schema.validate(req.body);

  if (error) {
    res.json({ error: error.details[0].message });
    return;
  } else {
    dbQuery(connection, addHighScoreQuery)
      .then((highscore) => {
        res.json(highscore);
      })
      .catch((error) => {
        next(error);
      });
    if (req.body.email) {
      mailService(req.body.email);
    }
  }
});

module.exports = router;
