const dotenv = require("dotenv");

dotenv.config();

module.exports = {
  port: process.env.PORT || 3000,
  mysql_host: process.env.MYSQL_HOST,
  mysql_name: process.env.MYSQL_NAME,
  mysql_password: process.env.MYSQL_PASSWORD,
  mysql_database: process.env.MYSQL_DATABASE,
  mailgun_api: process.env.MAILGUN_API,
  mailgun_domain: process.env.MAILGUN_DOMAIN,
};
